package com.example.colleagestudentrecruitsysterm.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.colleagestudentrecruitsysterm.R;
import com.example.colleagestudentrecruitsysterm.fragement.CompanyFragement;
import com.example.colleagestudentrecruitsysterm.fragement.InfomationFragment;
import com.example.colleagestudentrecruitsysterm.fragement.OccupationFragment;
import com.example.colleagestudentrecruitsysterm.fragement.OwnerFragement;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private FragmentPagerAdapter fragmentPagerAdapter;
    private BottomNavigationView navigationView;
    private List<Fragment> fragmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFragmentList();//初始化FragmentList
        initViewPager();//初始化ViewPager
        initBottomNV();//初始化BottomNavigationView
    }

    private void initFragmentList() {
        // 将fragment加载到list中
        fragmentList = new ArrayList<>();
        fragmentList.add(new OccupationFragment());
        fragmentList.add(new CompanyFragement());
        fragmentList.add(new InfomationFragment());
        fragmentList.add(new OwnerFragement());
    }

    private void initViewPager() {
        //实例化viewpager
        viewPager = findViewById(R.id.viewpager);
        //实例化FragmentPagerAdapter
        fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        };
        //设置viewpager的适配器
        viewPager.setAdapter(fragmentPagerAdapter);
        //设置viewpager的页面切换事件
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        navigationView.setSelectedItemId(R.id.occupation);
                        break;
                    case 1:
                        navigationView.setSelectedItemId(R.id.collection);
                        break;
                    case 2:
                        navigationView.setSelectedItemId(R.id.infomation);
                        break;
                    case 3:
                        navigationView.setSelectedItemId(R.id.owner);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void initBottomNV() {
        //实例化BottomNavigationView
        navigationView = findViewById(R.id.bottomnv);
        //设置BottomNavigationView切换事件
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@androidx.annotation.NonNull MenuItem item) {
                int id=0;
                switch (item.getItemId()) {
                    case R.id.occupation:
                        id = 0;
                        break;
                    case R.id.collection:
                        id = 1;
                        break;
                    case R.id.infomation:
                        id = 2;
                        break;
                    case R.id.owner:
                        id = 3;
                        break;

                }
                viewPager.setCurrentItem(id);
                return true;
            }
        });
    }


}