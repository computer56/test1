package com.example.colleagestudentrecruitsysterm.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.colleagestudentrecruitsysterm.R;

public class ChooseActivity extends AppCompatActivity {
    private TextView homepage,mypage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        initView();
        initEvent();
    }

    private void initView() {
        homepage = (TextView)findViewById(R.id.homepage);
        mypage = (TextView)findViewById(R.id.mypage);

    }

    private void initEvent() {
        homepage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        mypage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseActivity.this, MeFragment.class);
                startActivity(intent);
            }
        });

    }


}
