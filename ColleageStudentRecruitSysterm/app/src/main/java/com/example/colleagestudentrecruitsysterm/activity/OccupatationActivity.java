package com.example.colleagestudentrecruitsysterm.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.colleagestudentrecruitsysterm.R;

public class OccupatationActivity extends AppCompatActivity {
    ImageButton occupation_return;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_occupatation);
        initView();
        intiEvent();
    }

    private void intiEvent() {
        occupation_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initView() {
        occupation_return = (ImageButton)findViewById(R.id.occupation_return);
    }
}
