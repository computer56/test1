package com.example.colleagestudentrecruitsysterm.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.example.colleagestudentrecruitsysterm.R;


public class RegActivity extends AppCompatActivity  {
    private Button register;
    private ImageView reg_return;
    private TextView et_reg_user,et_reg_password,
            et_reg_repassword,et_reg_telephone,et_reg_comment,et_reg_adress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        initViews();
        initEvents();
    }

    private void initEvents() {
        //处理注册按钮
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        //处理返回按钮
        reg_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                finish();
            }
        });
    }

    private void initViews() {

        register = (Button)findViewById(R.id.register);
        reg_return = (ImageView)findViewById(R.id.reg_return);
        et_reg_user = (TextView)findViewById(R.id.et_reg_user);

        et_reg_comment = (TextView)findViewById(R.id.et_reg_comment);
        et_reg_adress = (TextView)findViewById(R.id.et_reg_adress);
        et_reg_telephone = (TextView)findViewById(R.id.et_reg_telephone);
        et_reg_repassword = (TextView)findViewById(R.id.et_reg_repassword);
        et_reg_password = (TextView)findViewById(R.id.et_reg_password);




    }





}