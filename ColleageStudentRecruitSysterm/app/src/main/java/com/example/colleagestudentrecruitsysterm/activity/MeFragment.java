package com.example.colleagestudentrecruitsysterm.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.colleagestudentrecruitsysterm.R;

public class MeFragment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mefragment);
    }
}
