package com.example.colleagestudentrecruitsysterm.fragement;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.colleagestudentrecruitsysterm.R;
import com.example.colleagestudentrecruitsysterm.activity.LoginActivity;
import com.example.colleagestudentrecruitsysterm.activity.MainActivity;
import com.example.colleagestudentrecruitsysterm.activity.OccupatationActivity;

public class OccupationFragment extends Fragment {
    View view;
    Button occupation_detail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment7, container, false);
        initview();
        initEvent();
        return view;
    }

    private void initEvent() {
        occupation_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OccupatationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initview() {
        occupation_detail =(Button)view.findViewById(R.id.occupation_detail);
    }
}
