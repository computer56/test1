# 大学生双向招聘系统

#### Description
第一步，公司发布招聘信息，公司按照平台提供的统一招聘信息表单，按要求填写招聘信息；如：职位，工资，工作具体要求，工作福利等。平台对招聘信息进行存储并发布出去。
第二步，学生招聘预约，学生向发布的招聘职位提交预约申请，并提交自己的简历，平台提供统一的简历表单，学生按要求填写简历信息。平台对学生的简历信息进行存储。
第三步，公司选择预约的学生，公司对于学生提交的预约申请进行选择。平台以列表的形式向公司显示学生提交预约情况，公司可以选择查看学生简历与个人信息来判断是否通过预约。
第四步，学生查看预约情况。学生可以在平台提供的个人信息页面查看自己提交的预约情况或者取消自己预约。平台将记录学生的预约请求。
需要说明，通过平台实现学生与公司之间信息流转的载体是电子表单（即学生个人简历，预约情况，公司信息，公司发布的招聘信息等）。


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
